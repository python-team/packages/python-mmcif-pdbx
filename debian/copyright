Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mmcif_pdbx
Upstream-Contact: https://github.com/Electrostatics/mmcif_pdbx/issues
Source: https://github.com/Electrostatics/mmcif_pdbx

Files: *
Copyright: 2020-2021, John Westbrook <jwest@rcsb.rutgers.edu>
License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along with
 this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the full text of the CC0 1.0 Universal license can be found
 in the file `/usr/share/common-licenses/CC0-1.0'.

Files: tests/data/*.cif
Copyright: Protein Data Bank
License: PDB
 Data files contained in the PDB archive (https://ftp.wwpdb.org) are free
 of all copyright restrictions and made fully and freely available for
 both non-commercial and commercial use. Users of the data should
 attribute the original authors of that structural data. By using the
 materials available in the PDB archive, the user agrees to abide by the
 conditions described in the PDB Advisory Notice.
 .
 ADVISORY NOTICE FOR USE OF THE PDB ARCHIVE
 .
 By using the materials available in this archive, the user agrees to abide
 by the following conditions:
 .
  * The archival data files in the PDB archive are made freely available
  to all users. Data files within the archive may be redistributed in original
  form without restriction. Redistribution of modified data files using the same
  file name as is on the FTP server is prohibited. The rules for file names are
  detailed at http://www.wwpdb.org/wwpdb_charter.html.
 .
  * Data files containing PDB content may incorporate the PDB 4-letter entry
  name (e.g. 1ABC) in standard PDB records only if they are exactly the same
  as what is residing in the PDB archive. This does not prevent databases
  from including PDB entry_id's as cross-references where it is clear that
  they refer to the PDB archive. PDB records refer to the standard PDB format.
  The distribution of modified PDB data including the records HEADER, CAVEAT,
  REVDAT, SPRSDE, DBREF, SEQADV, and MODRES in PDB format and their mmCIF and
  XML equivalents is not allowed.
 .
  * The user assumes all responsibility for insuring that intellectual property
  claims associated with any data set deposited in the PDB archive are honored.
  It should be understood that the PDB data files do not contain any information
  on intellectual property claims with the exception in some cases of a reference
  for a patent involving the structure.
 .
  * Any opinion, findings, and conclusions expressed in the PDB archive by the
  authors/contributors do not necessarily reflect the views of the wwPDB.
 .
  * The data in the PDB archive are provided on an "as is" basis.  The wwPDB
  nor its comprising institutions cannot be held liable to any party for direct,
  indirect, special, incidental, or consequential damages, including lost
  profits, arising from the use of PDB materials.
 .
  * Resources on this site are provided without warranty of any kind, either
  expressed or implied. This includes but is not limited to merchantability or
  fitness for a particular purpose. The institutions managing this site make
  no representation that these resources will not infringe any patent or other
  proprietary right.

Files: debian/*
Copyright: 2022, Andrius Merkys <merkys@debian.org>
License: LGPL-3+
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 3 can be found in "/usr/share/common-licenses/LGPL-3".
